``bumpversion``
===============

Manage versioning using ``bumpversion``.

Initialization
--------------

Installs ``bumpversion`` and creates (if not present) the ``.bumpversion.cfg``
file.

By default, the version is tracked in ``bricks.yml`` and ``setup.py``.

.. warning::

  Don't forget to add any other file in which the version appears in
  ``.bumpversion.cfg``.


Exposed commands
----------------

``bump-patch``
~~~~~~~~~~~~~~

Increments the patch version number.

``bump-minor``
~~~~~~~~~~~~~~

Increments the minor version number.

``bump-major``
~~~~~~~~~~~~~~

Increments the major version number.

