``dockercompose``
=================

Exposes base commands for managing a docker compose environment.

Initialization
--------------

Requires ``docker-compose`` in path.

Creates ``docker-compose.yml`` if it does not exist. The main service must
have the name of the project. (``metadata.name``)


Exposed commands
----------------

``start``
~~~~~~~~~

Starts the containers in the background


``stop``
~~~~~~~~

Stops all the containers

``logs``
~~~~~~~~

Shows the logs for the services

``tail``
~~~~~~~~

Shows the logs for the services and watches them. This command is blocking.

``build``
~~~~~~~~~

Force builds the services that have a build section.

``pull``
~~~~~~~~

Force pull of the images for the services that use a remote image

``exec``
~~~~~~~~

Executes the ``command`` command in the main container.
